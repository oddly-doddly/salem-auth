"use strict"

const path = require("path");
const fs = require('fs');
const bodyParser = require('body-parser');
const express = require('express');
const mongoose = require('mongoose');

const ROOT_PATH = '/api';
const config = require('./config/config');

let app = express();
let router = express.Router();
let port = process.env.Port || 8000;

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

/* SETUP DATABASE */
mongoose.Promise = require('bluebird');
mongoose.connect(config.database.url, {useMongoClient: true});

/* SETUP ROUTES */
fs.readdirSync(__dirname + '/routes').forEach(
    (file) => {
        module.exports[path.basename(file + '.js')] =
            require(path.join(__dirname + '/routes', file))(app, ROOT_PATH);
    }
);

router.get('/',
    (req, res) => {
        res.json({status: 'online', message: 'Salem-Auth is alive and well!'})
    });

app.use(ROOT_PATH, router).listen(port);
console.log("Salem-Auth service started at: ", "http://127.0.0.1:" + port + ROOT_PATH);